
import { Component, ElementRef, ViewChild } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.css' ]
})
export class AppComponent  {
  title = 'Mon Application';

  @ViewChild('canvas', { static: true })
  canvas!: ElementRef<HTMLCanvasElement>;
  
  private uploadedImage!: HTMLImageElement;
  isZoomed = false;

  ngOnInit(): void {
    
  }

  //téléchargement d'image
  animate(e?:any): void {
    const reader = new FileReader();

    reader.onloadend = (event) => {
      const img = new Image();
      const canvas = this.canvas.nativeElement;

      img.onload = () => {
        canvas.width = img.width;
        canvas.height = img.height;
      
        this.uploadedImage = img; // enregistrement d'image téléchargée pour les dimensions d'origine
        this.isZoomed = false;  // réinitialisement le zoom à 1x
        this.clearCanvas();
        this.drawImage();      
      }

      img.src = <string>event.target?.result;
    };

    if (e?.target.files[0]) {
      reader.readAsDataURL(e?.target?.files[0]);
    }
  }

  zoomIn(): void { // pour basculer zoom 1x - 2x
    if (this.uploadedImage) {
      this.isZoomed = !this.isZoomed;

      this.clearCanvas();
      this.drawImage();
    }
  }

  private drawImage(): void {
    const canvas = this.canvas.nativeElement;
    const ctx = canvas.getContext('2d');

    if (!ctx) {
      return;
    }

    const transX = canvas.width * 0.5;
    const transY = canvas.height * 0.5;
    ctx.translate(transX, transY); // pour déplacer l'origine au centre du canvas

    const img = this.uploadedImage; 

    const zoom = this.isZoomed ? 2.0 : 1.0; // le niveau de Zoom
    const w = img.width * zoom;
    const h = img.height * zoom;
    const x = -w / 2;
    const y = -h / 2;

    ctx.drawImage(img, 0, 0, img.width, img.height, x, y, w, h); // pour dessiner l'image

    ctx.setTransform(1, 0, 0, 1, 0, 0); // pour réinitialiser toutes les transformations
  }

  private clearCanvas(): void {
    const canvas = this.canvas.nativeElement;
    const ctx = canvas.getContext('2d');

    if (!ctx) {
      return;
    }

    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.rect(0, 0, canvas.width, canvas.height);
    ctx.fillStyle = 'blue';
    ctx.fill();
  }

}



